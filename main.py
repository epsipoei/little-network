#!/bin/usr/env python

from lib import terraform
from lib import docker
from lib import zabbix
from lib import interface

if __name__ == "__main__":
    args=interface.get_args()
    
    if args.init:
        terraform.terraform_init()
    if args.destroy:
        terraform.terraform_destroy()
    if args.rmi:
        docker.docker_images_remove()
    if args.build:
        docker.docker_images_build()
    if args.apply:
        terraform.terraform_apply()
        docker.docker_rsync_web()
    if args.start_zabbix:
        zabbix.configure_zabbix_agents()    
        zabbix.zabbix_instance_start()
    if args.start_docker:
        docker.docker_start()