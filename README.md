Little-Network
=
*Prerequis, you must install :*

- Docker    https://www.docker.com/ or RUN `docker-install.sh`  
- Terraform https://developer.hashicorp.com/terraform/downloads

*use `main.py` for create your Little Network:* 

usage: `main.py [-h] [-i] [-d] [-a] [-r] [-b] [-s]`

optional arguments:  
-  `-h` , `--help` : show this help message and exit.  
-  `-i` , `--init` : Run terraform init.  
-  `-d` , `--destroy` : Run terraform destroy.  
-  `-a` , `--apply` : Run terraform apply.  
-  `-r` , `--rmi` : Remove Docker images.  
-  `-b` , `--build` : Build Docker images.  
-  `-z` , `--start-zabbix` : Start Zabbix.
-  `-s` , `--start-docker` : Start Container docker  

_Little-Network is composed :_  
- `Apache-1`  --> `192.168.2.7`  
- `Apache-2`  --> `192.168.2.8`  
- `HAProxy`   --> `192.168.2.9`  // Load-balance //
- `Bind9`     --> `192.168.2.6` // tofix : edit /etc/resolv.conf dor use dns and add "nameserver 127.0.0.1"  
- `Dhcpd`    --> `192.168.2.4`  
- `Samba`     --> `192.168.2.5` // server path for smb-partage "`smb://192.168.2.5/partage`" user-default : "debian" ; mp: "iopiop"
- `Zabbix-web`    --> `192.168.2.3:8080` user:Admin mp:zabbix  
- `Zabbix-server` --> `192.168.2.3`

*Configure Web interface :*   
https://www.zabbix.com/documentation/6.4/en/manual/installation/frontend   
`!! If you use localhost you must replace localhost by 127.0.0.1 !!`   
INFO : Zabbix-agent is install by default on the containers, thanks enable Network discovery on zabbix

*You can use MYSQL Workbench for DB :* https://dev.mysql.com/downloads/workbench/   
- default user: zabbix   
- mp: zabbix

_Bibliography :_  

Provider Docker : https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs  
Zabbix : https://www.zabbix.com/download?zabbix=6.4&os_distribution=ubuntu&os_version=22.04&components=server_frontend_agent&db=mysql&ws=nginx

*TOFIX*   
TOFIX For use the dns please edit your /etc/resolv.conf for each vm and add "nameserver 192.168.2.2"
TOFIX dhcp run but dhcpd.conf is not set  
You can use acces ssh with user-default : "debian" ; mp: "iopiop" 
