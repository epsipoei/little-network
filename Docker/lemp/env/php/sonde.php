<?php
$user = "semaphp";
$password = "semaphp";
$database = "mspr";
$table = "SONDE";

try {
  $db = new PDO("mysql:host=192.168.2.10;dbname=$database", $user, $password);
  echo "<h2>SONDES</h2>";

  // Récupérer les données de la table
  $query = $db->query("SELECT s.num_sonde, s.nom_sonde, s.cpu_sonde, s.ram_sonde, d.taille_disque, s.etat_sonde 
                      FROM mspr.SONDE s
                      INNER JOIN DISQUE_SEMA d ON s.num_sonde = d.num_sonde");
  $rows = $query->fetchAll(PDO::FETCH_ASSOC);

  if (count($rows) > 0) {
    // Récupérer les en-têtes de colonnes
    $columns = array_keys($rows[0]);

    // Ajouter du CSS pour le tableau
    echo '<style>
            table {
              border-collapse: collapse;
              width: 50%;
              margin-left: 0;
            }
            th {
              background-color: lightgray;
              color: black;
            }
            td {
              background-color: cyan;
              color: black;
            }
            th, td {
              border: 1px solid black;
              padding: 8px;
              text-align: left;
            }
          </style>';

    echo "<table>";

    // Afficher les en-têtes de colonnes
    echo "<tr>";
    foreach ($columns as $column) {
      echo "<th>$column</th>";
    }
    echo "</tr>";

    // Récupérer les colonnes de données
    $dataColumns = array_column($rows, null, $columns[0]);

    // Afficher les données
    foreach ($dataColumns as $row) {
      echo "<tr>";
      foreach ($columns as $column) {
        echo "<td>" . htmlspecialchars($row[$column]) . "</td>";
      }
      echo "</tr>";
    }

    echo "</table>";
  } else {
    echo "Aucune donnée trouvée dans la table SONDE.";
  }
} catch (PDOException $e) {
  print "Error!: " . $e->getMessage() . "<br/>";
  die();
}
?>

