#!/bin/bash

mkdir /nonexistent

# Démarrer le service MySQL
service mysql start

# Créer la base de données Zabbix et l'utilisateur de base de données
sudo mysql << EOF
CREATE DATABASE mspr character set utf8mb4 collate utf8mb4_bin;
CREATE USER 'cma4'@'%' IDENTIFIED BY 'cma4';
GRANT ALL PRIVILEGES ON *.* TO 'cma4'@'%';
CREATE USER 'semaphp'@'%' IDENTIFIED WITH mysql_native_password BY 'semaphp';
GRANT ALL PRIVILEGES ON *.* TO 'semaphp'@'%';
FLUSH PRIVILEGES;
SET global log_bin_trust_function_creators = 1;
EOF
