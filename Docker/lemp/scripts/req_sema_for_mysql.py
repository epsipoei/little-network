import mysql.connector
import subprocess

# Paramètres de connexion à la base de données MySQL
db_host = '192.168.2.3'
db_user = 'zabbix'
db_password = 'zabbix'
db_name = 'mspr'

# Adresse IP de la machine distante
remote_ip = '192.168.2.100'

# Connexion à la base de données
conn = mysql.connector.connect(
    host=db_host,
    user=db_user,
    password=db_password,
    database=db_name
)
cursor = conn.cursor()

# Vérification de l'état de la machine distante
try:
    # Exécution d'une commande sur la machine distante pour vérifier son état
    result = subprocess.run(['ping', '-c', '1', remote_ip], capture_output=True, text=True)
    
    if result.returncode == 0:
        system_state = "allumé"
    else:
        system_state = "éteint"
    
    # Mise à jour de l'état dans la base de données
    cursor.execute("UPDATE SONDE SET etat_sonde = %s WHERE num_sonde = %s",
                   (system_state, 1))
    conn.commit()
    
    print("L'état de la machine a été mis à jour dans la base de données avec succès.")
except subprocess.CalledProcessError as error:
    print(f"Erreur lors de la vérification de l'état de la machine distante : {error}")

# Fermeture de la connexion à la base de données
cursor.close()
conn.close()
