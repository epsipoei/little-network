#!/bin/bash

# Démarrer le service MySQL
service mysql start

# Créer la base de données Zabbix et l'utilisateur de base de données
sudo mysql << EOF

drop database if exists mspr;
create database mspr;
use mspr;


-- tables relatives à la sonde et à ses caractéristiques techniques

create table SONDE (
num_sonde INT UNSIGNED,
nom_sonde VARCHAR(20),
cpu_sonde DECIMAL(2,1) NOT NULL,
ram_sonde SMALLINT NOT NULL,
id_semaos INT UNSIGNED NOT NULL,
etat_sonde ENUM('connectee', 'deconnectee', 'indefini') DEFAULT 'indefini',

CONSTRAINT CK_PK_SONDE PRIMARY KEY (num_sonde),
CONSTRAINT CHK_num_sonde CHECK (length(num_sonde) <= 10)
-- FK : SEMAOS(id_semaos)
);

create table DISQUE_SEMA (
id_disque INT UNSIGNED AUTO_INCREMENT, 
num_sonde INT UNSIGNED NOT NULL, 
type_disque VARCHAR(20) NOT NULL,
taille_disque SMALLINT NOT NULL,

CONSTRAINT CK_PK_disque_sema PRIMARY KEY (id_disque)
-- FK : SONDE(num_sonde)
);

create table SEMAOS (
id_semaos INT UNSIGNED AUTO_INCREMENT,
version_semaos VARCHAR(10) NOT NULL,
date_creation_os date NOT NULL,

CONSTRAINT CK_PK_SEMAOS PRIMARY KEY (id_semaos)
);

create table SCRIPT (
id_script INT UNSIGNED AUTO_INCREMENT,
nom_script VARCHAR(50) NOT NULL,
version_script VARCHAR(10) ,

CONSTRAINT CK_PK_SCRIPT PRIMARY KEY (id_script)
);
create table SCRIPT_SONDE (
num_sonde INT UNSIGNED, 
id_script INT UNSIGNED,
date_install_script date NOT NULL, 

CONSTRAINT CK_PK_SCRIPT_SONDE PRIMARY KEY (num_sonde, id_script)
-- FK : SONDE(num_sonde), SCRIPT(id_script)
);


-- Tables relatives au client

create table CLIENT (
id_client INT UNSIGNED AUTO_INCREMENT,
SIRET BIGINT,
nom_client VARCHAR(50) NOT NULL,
adresse_client VARCHAR(50) NOT NULL,
prestataire INT UNSIGNED,

CONSTRAINT CK_PK_client PRIMARY KEY (id_client),
CONSTRAINT CHK_SIRET CHECK (length(SIRET) = 14)
);

create table CONTACT (
id_contact INT UNSIGNED AUTO_INCREMENT,
nom_contact VARCHAR(50) NOT NULL,
id_client INT UNSIGNED NOT NULL,
tel_contact INT,
mail_contact VARCHAR(50),

CONSTRAINT CK_PK_CONTACT PRIMARY KEY (id_contact)
-- FK : CLIENT(id_client)
);

create table LOCATION (
id_client INT UNSIGNED,
num_sonde INT UNSIGNED,
date_deb_loc DATE NOT NULL,
date_fin_loc DATE,
date_recup_sonde DATE DEFAULT NULL,

CONSTRAINT CK_CPK_LOCATION PRIMARY KEY (id_client, num_sonde),
CONSTRAINT CHK_fin_loc CHECK (date_fin_loc > date_deb_loc),
CONSTRAINT CHK_recup_sonde CHECK (date_recup_sonde > date_deb_loc)
-- FK : SONDE(num_sonde), CLIENT(id_client)
);


-- Tables réseau

create table LAN (
id_LAN INT UNSIGNED AUTO_INCREMENT,
adresse_reseau VARCHAR(18) NOT NULL, 
nom_reseau VARCHAR(50),
id_client INT UNSIGNED,

CONSTRAINT CK_PK_LAN PRIMARY KEY (id_LAN)
-- FK : CLIENT(id_client)
);

create table RESEAU_SONDE (
num_sonde INT UNSIGNED,
id_LAN INT UNSIGNED,
ip_defaut_sonde VARCHAR(18) NOT NULL,

CONSTRAINT CK_PK_RESEAU_SONDE PRIMARY KEY (num_sonde, id_LAN)
-- FK : SONDE(num_sonde), LAN(id_LAN)
);


create table COMMUNICATION_VPN (
id_vpn INT UNSIGNED, 
num_sonde INT UNSIGNED, 
nom_tunnel VARCHAR(8),
adresse_client_VPN VARCHAR(18) NOT NULL,

CONSTRAINT CK_PK_COMMUNICATION_VPN PRIMARY KEY (id_vpn)
-- FK : SONDE(num_sonde)
);

-- Tables relatives aux opérations/incident

create table OPERATION_SONDE (
id_operation INT UNSIGNED AUTO_INCREMENT, 
num_sonde INT UNSIGNED, 
motif_operation ENUM('Dépannage ponctuel', 'Audit', 'Maintenance'),
type_operation ENUM('Redémarrage', 'Déploiement'),
id_employe INT UNSIGNED NOT NULL,
date_recup_sonde DATE DEFAULT NULL,
debut_operation TIMESTAMP DEFAULT NOW(),
fin_operation TIMESTAMP DEFAULT NULL,

CONSTRAINT CK_PK_OPERATION_SONDE PRIMARY KEY (id_operation),
-- FK : SONDE(num_sonde), EMPLOYE(id_employe)
CONSTRAINT CHK_fin_operation CHECK (fin_operation > debut_operation or fin_operation = NULL)
);

create table INCIDENT (
id_incident INT UNSIGNED AUTO_INCREMENT,
num_sonde INT UNSIGNED NOT NULL,
date_incident DATETIME NOT NULL,
id_operation INT UNSIGNED DEFAULT NULL,

CONSTRAINT CK_PK_INCIDENT PRIMARY KEY (id_incident)
-- FK : SONDE(num_sonde), OPERATION(id_operation)
);


create table EMPLOYE (
id_employe INT UNSIGNED AUTO_INCREMENT,
nom_employe VARCHAR(50) NOT NULL,
prenom_employe VARCHAR(50) NOT NULL,

CONSTRAINT CK_PK_EMPLOYE PRIMARY KEY (id_employe)
);

-- TEST : Insertion des données

INSERT INTO SONDE(num_sonde, cpu_sonde, ram_sonde, id_semaos, etat_sonde) 
VALUES (10101010, 5.5, 4, 1, 'Connectée'), (2020, 6, 16, 2, 'Déconnectée');
INSERT INTO SONDE(num_sonde, cpu_sonde, ram_sonde, id_semaos, etat_sonde) 
VALUES (30303030, 2.2, 12, 2, 'Déconnectée');


insert into disque_sema(num_sonde, type_disque, taille_disque)
values (10101010, 'sata', 16), (2020, 'IDE', 16), (2020, 'IDE', 16);

insert into semaos (version_semaos, date_creation_os) 
-- comment ajouter une contrainte pour que la date soit postérieure à celle de la version précédente ?
values ('1.12', '2003-12-12'), ('1.13', '2002-12-12');

insert into script(nom_script, version_script)
values ('script1', '1.0'), ('script2', '1.1');

insert into script_sonde (num_sonde, id_script, date_install_script)
values (10101010, 1, '2003-12-12'), (10101010, 2, '2004-12-12');

INSERT INTO CLIENT (SIRET, nom_client, adresse_client, prestataire)
values  (11111111111115,'pitt', 'adresse', 1), (11111111111112, 'roger', 'adresse', NULL),(11111111111111,'bob', 'adresse', 2);

insert into contact (nom_contact, id_client, tel_contact, mail_contact)
values ('bob', 1, 0122334455, 'bob@mail.com'), ('toto', 2, 0122334455, 'toto@mail.com') ;

insert into location (id_client, num_sonde, date_deb_loc, date_fin_loc)
values (1, 10101010, '2000-01-01', '2001-01-01'), (2, 2020, '2000-01-01', '2001-01-01'), (1, 2020, '2000-01-01', '2001-01-01');

insert into LAN (adresse_reseau, nom_reseau, id_client)
values ('192.168.10.1/24', 'reseau1', 1), ('192.168.2.1/12', 'reseau2', 2);

insert into RESEAU_SONDE
values (10101010, 1, '172.168.10.1/24');


insert into COMMUNICATION_VPN
values (1, 10101010, 'tunnel1','172.168.10.3/24' );

insert into OPERATION_SONDE (num_sonde, motif_operation, type_operation, id_employe, date_recup_sonde, debut_operation, fin_operation)
values (10101010, 'Maintenance', 'Redémarrage', 1, '2000-01-01', '2000-01-01', '2001-01-01'), (10101010, 'Maintenance', 'Redémarrage', 1, '2000-01-01', '2022-01-01', NULL);

-- Ajout de la durée de l'opération de maintenance (colonne duree_operation) dans la table OPERATION_SONDE : si la date de fin d'opération est précisée, on fait la différence entre la date de fin et la date de début, sinon on fait la différence avec la date du jour.
ALTER TABLE OPERATION_SONDE ADD duree_operation INT AFTER fin_operation;
UPDATE OPERATION_SONDE
SET duree_operation = IF(fin_operation IS NULL, TIMESTAMPDIFF(DAY, debut_operation, NOW()), TIMESTAMPDIFF(DAY, debut_operation, fin_operation));

insert into INCIDENT (num_sonde, date_incident, id_operation)
values (10101010, '2000-01-01', 1);

insert into EMPLOYE (nom_employe, prenom_employe)
values ('Bob', 'Toto');

-- Contraintes FK : placées en fin de script de sorte à pouvoir créer les tables/faire l'insertion des données dans n'importe quel ordre.

ALTER TABLE SONDE ADD CONSTRAINT CK_FK_SONDE_semaos FOREIGN KEY (id_semaos) REFERENCES SEMAOS(id_semaos);
ALTER TABLE DISQUE_SEMA ADD CONSTRAINT CK_FK_DISQUE_SEMA_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE SCRIPT_SONDE ADD CONSTRAINT CK_FK_SCRIPT_SONDE_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE SCRIPT_SONDE ADD CONSTRAINT CK_FK_SCRIPT_SONDE_SCRIPT FOREIGN KEY (id_script) REFERENCES SCRIPT(id_script) ON DELETE CASCADE;
ALTER TABLE CONTACT ADD CONSTRAINT CK_FK_CONTACT_CLIENT FOREIGN KEY (id_client) REFERENCES CLIENT(id_client) ON DELETE CASCADE;
ALTER TABLE LOCATION ADD CONSTRAINT CK_FK_LOCATION_SONDE FOREIGN KEY (num_sonde) REFERENCES sonde(num_sonde);
ALTER TABLE LOCATION ADD CONSTRAINT CK_FK_LOCATION_CLIENT FOREIGN KEY (id_client) REFERENCES CLIENT(id_client) ON DELETE CASCADE; 
ALTER TABLE LAN ADD CONSTRAINT CK_FK_LAN_CLIENT FOREIGN KEY(id_client) REFERENCES CLIENT(id_client) ON DELETE CASCADE;
ALTER TABLE RESEAU_SONDE ADD CONSTRAINT FK_RESEAU_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE RESEAU_SONDE ADD CONSTRAINT CK_FK_RESEAU_SONDE_RESEAU FOREIGN KEY (id_LAN) REFERENCES LAN(id_LAN) ON DELETE CASCADE;
ALTER TABLE COMMUNICATION_VPN ADD CONSTRAINT CK_FK_COMMUNICATION_VPN_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE OPERATION_SONDE ADD CONSTRAINT CK_FK_OPERATION_SONDE_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE OPERATION_SONDE ADD CONSTRAINT CK_FK_OPERATION_SONDE_EMPLOYE FOREIGN KEY (id_employe) REFERENCES EMPLOYE(id_employe);
ALTER TABLE INCIDENT ADD CONSTRAINT CK_FK_INCIDENT_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE INCIDENT ADD CONSTRAINT CK_FK_INCIDENT_OPERATION FOREIGN KEY (id_operation) REFERENCES OPERATION_SONDE(id_operation) ON DELETE SET NULL;


-- Contrainte sur l'association réflexive de la table CLIENT : placée après l'insertion des données afin de pouvoir ajouter des clients finaux ou des prestataires dans n'importe quel ordre.
-- Renvoie une erreur si le SIRET n'est pas composé de 14 chiffres et si la valeur de la colonne prestataire ne renvoie pas à une entrée de la table CLIENT

ALTER TABLE CLIENT ADD CONSTRAINT CK_FK_CLIENT_prestataire FOREIGN KEY (prestataire) REFERENCES CLIENT(id_client);


-- Index
CREATE INDEX IDX_SONDE_nom ON SONDE (nom_sonde);
EOF