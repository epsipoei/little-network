# Set up the Docker provider with the host location
provider "docker" {
    host = "unix:///var/run/docker.sock"
}

# Create a Docker network resource with a specified name and driver,
# and set the IP address and subnet range
resource "docker_network" "local" {
    name   = var.network
    driver = "bridge"
    ipam_config {
        gateway   = "192.168.2.1"
        ip_range  = "192.168.2.2/24"
        subnet    = "192.168.2.0/24"
    }
}

# Get Docker image data for each specified image using data sources
data "docker_image" "images" {
    for_each = var.images
    name = each.key
}

# Create Docker containers for each specified image, setting the container
# name, hostname, image ID, and IP address on the network
resource "docker_container" "containers" {
    for_each = var.images
    name      = each.value.name
    hostname  = each.value.name
    image     = data.docker_image.images[each.key].id
    networks_advanced {
        name = var.network
        ipv4_address = each.value.ip
    }
}