variable "network" {
    default = "local"
    type = string
}

variable "images" {
    default = {
        "my_zabbix:latest" = {
            name  = "zabbix"
            ip    = "192.168.2.3"
        },
        "my_dhcpd:latest" = {
            name  = "dhcpd"
            ip    = "192.168.2.4"
        },
        "my_samba:latest" = {
            name  = "samba"
            ip    = "192.168.2.5"
        },
        "my_bind9:latest" = {
            name  = "bind9"
            ip    = "192.168.2.6"
        },
        "my_apache-1:latest" = {
            name  = "apache-1"
            ip    = "192.168.2.7"
        },
        "my_apache-2:latest" = {
            name  = "apache-2"
            ip    = "192.168.2.8"
        },
        "my_ha-proxy:latest" = {
            name  = "ha-proxy"
            ip    = "192.168.2.9"
        },
        "my_lemp:latest" = {
            name  = "lemp"
            ip    = "192.168.2.10"
        }
    }
}
