#!/bin/usr/env python

import os
import subprocess

"""
This script contains functions to initialize, deploy, and destroy Terraform resources.

Functions:
- terraform_init: This function initializes Terraform using the 'terraform init' command.
- terraform_apply: This function deploys Terraform resources using the 'terraform apply' command.
- terraform_destroy: This function destroys Terraform resources using the 'terraform destroy' command with the '-auto-approve' option.
"""

# Function to initialize Terraform
def terraform_init():
    os.system("sudo terraform init")

# Function to destroy Terraform resources
def terraform_destroy():
    os.system("sudo terraform destroy -auto-approve")

# Function to deploy Terraform resources
def terraform_apply():
    os.system("sudo terraform apply -auto-approve")
    subprocess.run(["docker", "exec", "-it", "dhcpd", "bash", "./services_start.sh"])