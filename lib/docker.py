#!/bin/usr/env python

import os
import subprocess
from lib import secret

"""
This script contains several functions related to building and managing Docker images and configuring secure SSH access to a Docker container.

Functions:
- docker_images_remove: This function removes existing Docker images.
- docker_images_build: This function builds Docker images using Dockerfiles.
- docker_rsync_web: This function sets up secure SSH access to a Docker container and copies the public key to a specified location.
"""

# Function to remove Docker images
def docker_images_remove():
    os.system("sudo docker rmi \
        my_debian \
        my_apache-1 \
        my_apache-2 \
        my_bind9 \
        my_dhcpd \
        my_ha-proxy \
        my_samba \
        my_ubuntu \
        my_lemp \
        my_zabbix")

# Function to build Docker images
def docker_images_build():
    os.system("sudo docker build -t my_debian ./Docker/debian_template")
    os.system("sudo docker build -t my_ubuntu ./Docker/ubuntu_template")
    os.system("sudo docker build -t my_apache-1 ./Docker/apache-1/")
    os.system("sudo docker build -t my_apache-2 ./Docker/apache-2/")
    os.system("sudo docker build -t my_bind9 ./Docker/bind9/")
    os.system("sudo docker build -t my_dhcpd ./Docker/dhcpd/")
    os.system("sudo docker build -t my_ha-proxy ./Docker/ha-proxy/")
    os.system("sudo docker build -t my_samba ./Docker/samba/")
    os.system("sudo docker build -t my_zabbix ./Docker/zabbix")
    os.system("sudo docker build -t my_lemp ./Docker/lemp")

# Function to set up secure SSH access to a Docker container and copy the public key to a 
def docker_rsync_web():
    keygen = "ssh-keygen -t rsa -b 4096 -N '' -f /root/.ssh/id_rsa"
    ssh_copy_id = f"sshpass -p '{secret.Pass_user_apache2}' ssh-copy-id -i /root/.ssh/id_rsa.pub \
        -o StrictHostKeyChecking=no {secret.User_apache2}@{secret.IP_apache2}"
        
    subprocess.run(["docker", "exec", "-i", "apache-1", "sh", "-c", keygen])
    subprocess.run(['docker', 'exec', '-i', 'apache-1', 'bash', '-c', ssh_copy_id])

# Function to start container
def docker_start():
    os.system("docker start $(docker ps -aqf 'status=exited')")
    subprocess.run(["docker", "exec", "-it", "dhcpd", "bash", "./services_start.sh"])