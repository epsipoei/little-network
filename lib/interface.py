#!/bin/usr/env python

import argparse

"""
This script contains functions for managing Docker and Terraform resources, as well as starting Zabbix.

Functions:
- docker_images_remove: This function removes Docker images.
- docker_images_build: This function builds Docker images.
- terraform_init: This function initializes Terraform.
- terraform_destroy: This function destroys Terraform resources.
- terraform_apply: This function deploys Terraform resources.
- get_args: This function parses command line arguments for the script.
"""

def get_args():
    parser = argparse.ArgumentParser(description="usage: script.py [-h] [-i] [-d] [-a] [-r] [-b] [-z] [-s]")
    parser.add_argument("-i", "--init",
                        help="Terraform init",
                        action="store_true")
    parser.add_argument("-d", "--destroy",
                        help="Terraform destroy",
                        action="store_true")
    parser.add_argument("-a", "--apply",
                        help="Terraform apply",
                        action="store_true")
    parser.add_argument("-r", "--rmi",
                        help="Docker image remove",
                        action="store_true")
    parser.add_argument("-b", "--build",
                        help="Docker image build",
                        action="store_true")
    parser.add_argument("-s", "--start-docker",
                        help="Docker start container",
                        action="store_true")
    parser.add_argument("-z", "--start-zabbix",
                        help="Start Zabbix",
                        action="store_true")

    return parser.parse_args()
