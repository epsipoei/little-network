#!/bin/usr/env python

import os
import subprocess

"""
This script contains functions to configure Zabbix agents and start a Zabbix instance.

Functions:
- configure_zabbix_agents: This function configures Zabbix agents for each Docker container by changing the hostname in the agent configuration file.
- zabbix_instance_start: This function starts a Zabbix instance by running a script inside the corresponding Docker container.
"""

# Function to configure Zabbix agents for each Docker container
def configure_zabbix_agents():
    os.system("sudo docker exec -it apache-1 sed -i 's/Hostname=Zabbix server/Hostname='$(hostname -f)'/g' /etc/zabbix/zabbix_agent2.conf")
    os.system("sudo docker exec -it apache-2 sed -i 's/Hostname=Zabbix server/Hostname='$(hostname -f)'/g' /etc/zabbix/zabbix_agent2.conf")
    os.system("sudo docker exec -it ha-proxy sed -i 's/Hostname=Zabbix server/Hostname='$(hostname -f)'/g' /etc/zabbix/zabbix_agent2.conf")
    os.system("sudo docker exec -it bind9 sed -i 's/Hostname=Zabbix server/Hostname='$(hostname -f)'/g' /etc/zabbix/zabbix_agent2.conf")
    os.system("sudo docker exec -it samba sed -i 's/Hostname=Zabbix server/Hostname='$(hostname -f)'/g' /etc/zabbix/zabbix_agent2.conf")
    os.system("sudo docker exec -it dhcpd sed -i 's/Hostname=Zabbix server/Hostname='$(hostname -f)'/g' /etc/zabbix/zabbix_agent2.conf")

# Function to start the Zabbix instance
def zabbix_instance_start():
    subprocess.run(["docker", "exec", "-it", "zabbix", "bash", "./services_start.sh"])
